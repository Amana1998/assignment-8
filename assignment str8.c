#include <stdio.h>
#include<string.h>

struct student { 
    char name[50];
    char sub[25];
    int mark;
};

struct student s[5];

int main() {
    int i;
    for(i=0;i<5;i++){
        printf("Enter student %d name:",i+1);
        scanf(" %[^\n]s",s[i].name);

        printf("Enter student %d subject:" ,i+1);
        scanf(" %[^\n]s",s[i].sub);

        printf("Enter student %d mark :" ,i+1);
        scanf("%d",&s[i].mark);

    }
    for(i=0;i<5;i++){
        printf("\n");
        printf("Student %d\n ", i+1);
        printf("Student %d Name is %s\n",i+1,s[i].name);
        printf("Student %d Subject is %s\n",i+1,s[i].sub);
        printf("Student %d Marks is %d\n",i+1,s[i].mark);
        printf("\n");
    }
}
